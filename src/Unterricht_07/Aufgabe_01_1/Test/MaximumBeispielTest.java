package Unterricht_07.Aufgabe_01_1.Test;

import Unterricht_07.Aufgabe_01_1.MaximumBeispiel;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class MaximumBeispielTest {

    @Test
    public void testThatSimonDidWell() {
        int bla[][] = {{1,2},{30,4},{5,6}};

        assertEquals(MaximumBeispiel.berechneMaxWert(bla), 30);

    }

}

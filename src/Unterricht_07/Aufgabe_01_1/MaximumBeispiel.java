package Unterricht_07.Aufgabe_01_1;

public class MaximumBeispiel {

    public static void main(String[] args) {

        int bla[][] = {{1,2},{30,4},{5,6}};

        System.out.println(berechneMaxWert(bla));

    }

    public static int berechneMaxWert(int[][] umsatz) {
        int max = 0;
        for (int i = 0; i < umsatz.length; i++) {
            for (int j = 0; j < umsatz[i].length; j++) {
                if (umsatz[i][j] > max) {
                    max = umsatz[i][j];
                }
            }
        }
        return max;
    }

}

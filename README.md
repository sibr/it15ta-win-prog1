![](https://www.zhaw.ch/storage/engineering/ueber-uns/medien/Logo-School-of-Engineering-Deutsch.jpg)

# README

Dieses Repository dient zur Zusammenarbeit für die Übungen des IT15ta-win Studiengangs an der ZHAW.

## Setup

Dieses Projekt verwendet Git als DVCS. Git wird auf dem Mac am einfachsten mit dem Paketmanager [brew](http://brew.sh) installiert. Brew wird via Konsole installiert: 
```
#!bash
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
Anschliessend kann Git mit brew installiert werden:
```
#!bash
brew install git
```
Für eine Installation auf Linux und Windows siehe [Git-Dokumentation](http://git-scm.com/book/de/v1/Los-geht’s-Git-installieren).

Je nach IDE kann nun aus dem URI des Repository direkt ein Projekt erstellt werden. Die Installation ist in IntelliJ und Netbeans weitgehend selbsterklärend. Ansonsten finden sich hier Tutorials für [IntelliJ](https://www.jetbrains.com/idea/help/setting-up-a-local-git-repository.html) und für [Netbeans](https://netbeans.org/kb/docs/ide/git.html). Die Integration bei Eclipse ist leider ziemlich konfus und mir nur als Workaround bekannt. Dazu wird das Repository erst geklont und der Inhalt in ein bestehendes Eclipse-Projekt kopiert.

## Wie wird dieses Projekt genutzt?
Für jede Übung kann ein neues Package erstellt werden. Getrackt werden bisher nur relevante Files für Git und das src Verzeichnis. IDE spezifische Projekteinstellungen bleiben bis auf Weiteres lokal beim User. Für die Benutzung von Git wird entweder das CLI benutzt oder eine GUI-Anwendung wie bspw. [Sourcetree](http://www.sourcetreeapp.com). Manche IDEs bieten auch eine Integration direkt in der Arbeitsumgebung an.